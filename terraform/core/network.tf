resource "aws_vpc" "main" {
  cidr_block                       = var.ops_vpc_cidr_block
  enable_dns_support               = true
  enable_dns_hostnames             = true
  assign_generated_ipv6_cidr_block = false

  tags = {
    Name        = "main-${var.region}-${var.environment}"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_internet_gateway" "main_gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name        = "main-${var.region}-${var.environment}"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_route_table" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name        = "main-${var.region}-${var.environment}"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_main_route_table_association" "main" {
  vpc_id         = aws_vpc.main.id
  route_table_id = aws_route_table.main.id
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gw.id
  }

  tags = {
    Name        = "public-${var.region}-${var.environment}"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_subnet" "opsnet_pub" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.opsnet_pub
  map_public_ip_on_launch = true
  availability_zone       = var.availability_zones_pub

  tags = {
    Name        = "opsnet_public"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.opsnet_pub.id
  route_table_id = aws_route_table.public.id
}

resource "aws_subnet" "opsnet_private" {
  count                   = length(split(",", var.opsnet_private))
  vpc_id                  = aws_vpc.main.id
  cidr_block              = element(split(",", var.opsnet_private), count.index)
  map_public_ip_on_launch = false
  availability_zone       = element(split(",", var.availability_zones_private), count.index)

  tags = {
    Name        = "opsnet_private-${count.index + 1}"
    Region      = var.region
    Environment = var.environment
  }
}

resource "aws_route_table_association" "main" {
  count          = length(split(",", var.opsnet_private))
  subnet_id      = element(aws_subnet.opsnet_private.*.id, count.index)
  route_table_id = aws_route_table.main.id
}


resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "gw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.opsnet_pub.id
}

resource "aws_route" "nat-route" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.gw.id
}

output "vpc_main_id" {
  value = aws_vpc.main.id
}

output "opsnet_pub" {
  value = aws_subnet.opsnet_pub.id
}

output "opsnet_private" {
  value = aws_subnet.opsnet_private.*.id
}
