terraform {
  backend "s3" {
    bucket         = "terraform-tk-us-east-1-dev"
    key            = "core-terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform-state-locking-v1"
  }
}