resource "aws_key_pair" "ops-provisioner" {
  key_name   = "dev"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgnoUDuhwBY3IxhBr3seWT5xEEwPlTZHQ25fhpC6JUFuZBMZcaiMhXYYFrRJhbWVnmkZytKD7a+MzROOPBxsa/ujCU89JyaNSiMCTuS3DeVgGIjljIzjr4HmPxEgcsOmN0VCIC1GCT9/fFQvgWFnc54lCLwAbYKL2MxJIkUexamnJjmKP68+riLmL2H3XIQdmdZDsT2JomZ9CMi5B96LtA5uomnLuKiLqH34x8bOv0E8l/7S409DDUtdUigpAlQONiT2gsATC8kWAtxV/Qj1kx1r/PyvP3l0FQp6xI+5zg4IDZUS6tCwJNSKNonSIRI9iewu9crFOjeA8dLq4DZ4J5"
}
output "ops_prov_key" {
  value = aws_key_pair.ops-provisioner.key_name
}

