#Create SG for LB, only TCP/80,TCP/443 and outbound access
resource "aws_security_group" "dev-sg" {
  name        = "${var.region}-${var.environment}-sg"
  description = "Allow 443 and traffic to Jenkins SG"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "Allow 443 from anywhere"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow 80 from anywhere for redirection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "aws_security_group" {
  value = aws_security_group.dev-sg.id
}