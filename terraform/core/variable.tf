
variable "access_key" {
  default = ""
}

variable "secret_key" {
  default = ""
}

variable "region" {
  default = "us-east-1"
}

variable "environment" {
  default = "dev"
}


variable "ops_vpc_cidr_block" {
  default = "10.30.0.0/16"
}



variable "opsnet_pub" {
  default = "10.30.0.0/24"
}


variable "opsnet_private" {
  default = "10.30.1.0/24,10.30.2.0/24"
}
variable "availability_zones_pub" {
  default = "us-east-1a"
}

variable "availability_zones_private" {
  default = "us-east-1b,us-east-1c"
}
