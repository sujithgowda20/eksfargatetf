 resource "aws_s3_bucket" "tfbucket" {
  bucket = "terraform-tk-${var.region}-${var.environment}"
  acl    = "private"

  # force_destroy = true

  versioning {
    enabled = true
  }
  tags = {
    Name        = "state-bucket"
    Environment = var.environment
  }
}

data "template_file" "state-bucket-policy" {
  template = file("${path.module}/files/state-bucket-policy.tpl.json")

  vars = {
    region      = var.region
    environment = var.environment
  }
}

resource "aws_s3_bucket_policy" "tfbucketpolicy" {
  bucket = aws_s3_bucket.tfbucket.id
  policy = data.template_file.state-bucket-policy.rendered
} 