
variable "access_key" {
}

variable "secret_key" {
 
}

variable "region" {
  default = "us-east-1"
}

variable "environment" {
  default = "dev"
}

variable "name" {
  default = "tfstate"
}


