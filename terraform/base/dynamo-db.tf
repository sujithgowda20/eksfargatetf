resource "aws_dynamodb_table" "terraform-state-locking" {
  name           = "terraform-state-locking-v1"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "terraform-state-locking"
    Region      = var.region
    Environment = var.environment
  }
}