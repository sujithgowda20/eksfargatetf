data "terraform_remote_state" "core" {
  backend = "s3"

  config = {
    bucket         = "terraform-tk-us-east-1-dev"
    key            = "core-terraform.tfstate"
    region         = var.region
    dynamodb_table = "terraform-state-locking-v2"
    #access_key = var.access_key
    #secret_key = var.secret_key
  }
}

locals {
  vpc_main_id           = data.terraform_remote_state.core.outputs.vpc_main_id
  private-subnet-id-one = data.terraform_remote_state.core.outputs.opsnet_private[0]
  private-subnet-id-two = data.terraform_remote_state.core.outputs.opsnet_private[1]
  public-subnet-id      = data.terraform_remote_state.core.outputs.opsnet_pub
  ops_prov_key          = data.terraform_remote_state.core.outputs.ops_prov_key
  security-group        = data.terraform_remote_state.core.outputs.aws_security_group

}
