resource "aws_eks_fargate_profile" "default_namespaces" {
  depends_on             = [aws_eks_cluster.this]
  cluster_name           = aws_eks_cluster.this.name
  fargate_profile_name   = "default_namespaces"
  pod_execution_role_arn = aws_iam_role.eks-pod-fargate.arn
  subnet_ids             = [local.private-subnet-id-one, local.private-subnet-id-two]
  timeouts {
    delete = "30m"
  }
  selector {
    namespace = "default"
  }
  selector {
    namespace = "kube-system"
  }
}

data "aws_eks_cluster" "main" {
  name = aws_eks_cluster.this.id
}

data "aws_eks_cluster_auth" "main" {
  name = aws_eks_cluster.this.id
}

data "tls_certificate" "main" {
  url = aws_eks_cluster.this.identity[0].oidc[0].issuer
}

data "template_file" "kubeconfig" {
  template = <<EOF
apiVersion: v1
kind: Config
current-context: terraform
clusters:
- name: "${aws_eks_cluster.this.name}"
  cluster:
    certificate-authority-data: "${aws_eks_cluster.this.certificate_authority.0.data}"
    server: "${aws_eks_cluster.this.endpoint}"
contexts:
- name: terraform
  context:
    cluster: "${aws_eks_cluster.this.name}"
    user: terraform
users:
- name: terraform
  user:
    token: "${data.aws_eks_cluster_auth.main.token}"
EOF
}


resource "local_file" "kubeconfig" {
  content  = data.template_file.kubeconfig.rendered
  filename = "/root/.kube/config"
}

resource "null_resource" "coredns_patc" {
  provisioner "local-exec" {
    command     = "/bin/bash -x ./coredns.sh"
  }
}


resource "aws_iam_openid_connect_provider" "cluster" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.main.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.this.identity[0].oidc[0].issuer
}



