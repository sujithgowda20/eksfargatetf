#!/bin/bash

kubectl patch -n kube-system deployment/coredns --type=json --patch '[{"op": "remove", "path": "/spec/template/metadata/annotations", "value": "eks.amazonaws.com/compute-type"}]'
echo "patch updated"
sleep 3
kubectl rollout restart -n kube-system deployment/coredns
