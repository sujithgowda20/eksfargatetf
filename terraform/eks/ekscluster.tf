resource "aws_eks_cluster" "this" {
  name     = var.eks-cluster-name
  role_arn = aws_iam_role.eks-cluster.arn
  version  = var.kubernetes-version

  vpc_config {
    subnet_ids         = [local.private-subnet-id-one, local.private-subnet-id-two, local.public-subnet-id]
    security_group_ids = list(local.security-group)
  }

  depends_on = [
    aws_iam_role_policy_attachment.eks-cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.eks-cluster-AmazonEKSServicePolicy,
  ]
}

output "endpoint" {
  description = "AWS EKS cluster (Kubernetes control plane) endpoint"
  value       = aws_eks_cluster.this.endpoint
}

output "kubeconfig-certificate-authority-data" {
  description = "AWS EKS cluster (Kubernetes control plane) kubeconfig"
  value       = aws_eks_cluster.this.certificate_authority.0.data
}
